import React, { useEffect, useState } from "react";
import { Text, StyleSheet, View, Image, ScrollView, TouchableOpacity, StatusBar, SafeAreaView } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { selectors, actions } from "./store/inventory";
import { StackScreenProps } from "@react-navigation/stack";
import { StackParamList } from "./App";
import Icon from 'react-native-vector-icons/Ionicons';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { FAB } from "react-native-paper";

export default (props: StackScreenProps<StackParamList, "ProductItem">) => {

    const inventory = useSelector(selectors.selectInventory);
    const dispatch = useDispatch();
    const [state, setState] = useState([]);
    
    useEffect(() => {
        const unsubscribe = props.navigation.addListener("focus", () => {
        dispatch(actions.fetchInventory());
        });
        return unsubscribe;
    }, [props.navigation]);

    function getImage(url: String) {
        if(url==null || url==undefined) {
            return require('../assets/no-img.jpg')
        } else {
            return { uri: url };
        }
    }

    function isNew(date: Date) {
        const today = new Date();
        const checkDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        return Math.floor(Math.abs(checkDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24));
    }

    return (
        <View style={styles.main}>
            <ScrollView >
                <StatusBar  backgroundColor = "#EFEFEE" />  
                <Text style={styles.h1}>Inventory</Text>

                {inventory.map((record, index) => {
                    
                    if(record.fields["Product Name"]) {

                        const tags = record.fields["Product Categories"] ? record.fields["Product Categories"].split(',') : [];

                        function deploy(id:string) {
                
                            const data = {...state}

                            if(state.hasOwnProperty(id)) {
                                delete data[id];
                            } else {
                                data[id] = !record.deploy
                            }
                            setState(data)
                        }

                        return(
                            <View key={index} style={styles.box}>
                                <Image style={styles.image} source={getImage(record.fields['Product Image'])} />
                                <View style={styles.content}>
                                    <View style={styles.header}>
                                        <Text style={styles.title} numberOfLines={1}>{record.fields["Product Name"]}</Text>
                                        <View style={styles.headerRight}>
                                            {isNew(new Date(record.fields.Posted)) <= 7 && 
                                                <View style={styles.new}>
                                                    <Text style={{color: '#DADEE1'}}>NEW</Text>
                                                </View> 
                                            }
                                            {!state.hasOwnProperty(record.id) ? (
                                                
                                                <TouchableOpacity
                                                    onPress={() => deploy(record.id)}
                                                >
                                                    <Icon 
                                                        name="chevron-down-outline"
                                                        size={25}
                                                        color='#616161'
                                                    ></Icon>
                                                </TouchableOpacity>
                                                
                                            ) : (

                                                <TouchableOpacity
                                                    onPress={() => deploy(record.id)}
                                                >
                                                    <Icon 
                                                        name="chevron-up-outline"
                                                        size={25}
                                                        color='#616161'
                                                    ></Icon>
                                                </TouchableOpacity>
                                            )}
                                        </View>
                                    </View>
                                    <View><Text style={{color: '#242424'}}>{new Date(record.fields.Posted).toLocaleDateString()}{" "}</Text></View>
                                    
                                    {state.hasOwnProperty(record.id) &&
                                        <View style={styles.tags}> 
                                        {tags.map((tag, index2) => (
                                            <Text key={index2} style={styles.tag}>{tag}</Text>
                                        ))}
                                        </View>
                                    }
                                </View>
                            </View>
                        );
                    }
                })}
            </ScrollView>
            <SafeAreaView style={styles.fab}>
                <FAB
                    icon={() => (
                        <MaterialCommunityIcons name="barcode" size={24} color="#0B5549" />
                    )}
                    label="Scan Product"
                    onPress={() => props.navigation.navigate("Camera")}
                />
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
      flex: 1,
      backgroundColor: '#EFEFEE',
      paddingTop: StatusBar.currentHeight,
      paddingVertical: 20
    },
    h1: {
        textAlign: 'center',
        marginVertical: 28,
        fontWeight: '600',
        fontSize: 25
    },
    box: {
        minHeight: 100,
        flex: 1,
        flexDirection: 'row',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#BDBDBD',
        marginTop: 10,
        padding: 5,
        backgroundColor: '#EFEFEE',
        marginHorizontal: 15
    },
    image: {
        flex: 1,
        marginRight: 5,
        maxHeight: 300
    },
    tags: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 1,
        marginTop: 10
    },
    tag: {
        backgroundColor: '#D2E8FA',
        padding: 5,
        borderRadius: 20,
        margin: 5
    },
    new: {
        backgroundColor: '#2B2928',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        padding: 5,
        paddingHorizontal: 10,
        marginRight: 7
    },
    content: {
        flex: 2,
        color: '#CCD6E0'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    headerRight: {
        flexDirection: 'row',
    },
    title: {
        fontSize: 18,
        color: '#242424',
        flex: 5,
        fontWeight: '800',
        marginRight: 5
    },
    fab: {
        position: "absolute",
        bottom: 16,
        width: "100%",
        flex: 1,
        alignItems: "center"
    }
});